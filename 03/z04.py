"""
 * Dodatni zadatak.
"""
import nltk
from nltk.book import *

def once(text):
    fd = FreqDist(text)
    jednom = [w for w in set(text) if fd[w] == 1]
    return jednom

jednom = once(text1)
print(jednom)
print("Broj rijeci koje se pojavljuju samo jednom:", len(once(text1)))
