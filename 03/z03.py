"""
 * Definirajte funkciju vocab_size(text) kojoj je argument ime tekstualnog objekta,
   a koja vrada broj pojavnica u rječniku napravljen od danog teksta. (0,5)
"""
import nltk
from nltk.book import *

def vocab_size(text):
    return len(set(text))

print("Broj pojavnica:", vocab_size(text1))
