"""
 * Definirajte funkciju lexical_diversity(text) kojoj će argument
   biti ime tekstualnog objekta, a koja računa prosječno
   pojavljivanje riječi u tekstu (isprobajte s text4) (0,25)
"""
import nltk
from nltk.book import *

def lexical_diversity(text) :
    return len(text)/len(set(text))

prosjek = lexical_diversity(text4)
print(prosjek)
