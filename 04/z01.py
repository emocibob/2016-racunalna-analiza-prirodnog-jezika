"""
 * Pronađite frekventnost pojavljivanja dva samoglasnika jedan
   iza drugoga u prozivoljnom tekstu na hrvatskom. (0,5)
"""
import nltk
import re

with open('hr_tekst.txt', 'r') as infile:
    tekst = infile.read()

finds = re.findall('[aeiuo]{2}', tekst)
fd = nltk.FreqDist(finds)
print(fd.items())
