"""
 * Učitajte proizvoljni tekst iz korpusa na engleskom jeziku, “tokenizirajte”
   ga i ispišite  sve riječi koje počinju s “wh” sortirane po abecedi. (0,5)
"""
import nltk

with open('en_text.txt', 'r') as infile:
    text = infile.read()

tokens = nltk.word_tokenize(text)

wh_tokens = []
for token in tokens:
    if token[:2] == 'wh':
        wh_tokens.append(token)

wh_tokens.sort()
print(wh_tokens)
