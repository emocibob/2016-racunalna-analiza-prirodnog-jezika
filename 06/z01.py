"""
 * Nađite popis muških i ženskih hrvatskih imena na internetu s dostupnih stranica
   te napravite dokument s muškim i ženskim imenima koji će poslužiti klasifikaciji.
 * Naučite i testirajte sljedeće klasifikatore za klasificiranje hrvatskih imena na muške i ženske:
   - Naivni Bayesov klasifikator
   - Klasifikator maksimalne entropije
   - Klasifikator stabla odlučivanja
 * Provjerite točnost klasifikatora i za svaki nađite 10 najinformativnijih značajki (3 boda)
"""
from nltk import NaiveBayesClassifier
from nltk import classify
from nltk import MaxentClassifier
from nltk import DecisionTreeClassifier
import random


def gender_features_1(ime):
    return {'last_letter': ime[-1]}
    
def gender_features_2(ime):
    
    features = {}
    features['firstletter'] = ime[0].lower()
    features['lastletter'] = ime[-1].lower()
    # Paznja 1: zbog ime[0] i ime[-1] ne gledaju se dupla slova kao npr. 'nj'
    # Paznja 2: neka slova se broje previse puta kao npr. 'n' u 'nj'
    for slovo in slova:
        features['count(%s)' % slovo] = ime.lower().count(slovo)
        features['has(%s)' % slovo] = (slovo in ime.lower())
    
    return features
     

slova = 'A B C Č Ć D DŽ Đ E F G H I J K L LJ M N NJ O P R S Š T U V Z Ž'
slova = slova.split()
slova = [s.lower() for s in slova]

with open('muska_imena.txt', 'r', encoding='utf-8') as infile:
    muska_imena = infile.read().split()

with open('zenska_imena.txt', 'r', encoding='utf-8') as infile:
    zenska_imena = infile.read().split()


data = [(m, 'M') for m in muska_imena] + [(z, 'Z') for z in zenska_imena]
random.shuffle(data)
limit = int(len(data) * 0.9)

features_1 = [(gender_features_1(ime), spol) for (ime, spol) in data]
train_set_1, test_set_1 = features_1[:limit], features_1[limit:]

features_2 = [(gender_features_2(ime), spol) for (ime, spol) in data]
train_set_2, test_set_2 = features_2[:limit], features_2[limit:]

'''
Naive Bayes
'''

nb_classifier_1 = NaiveBayesClassifier.train(train_set_1)
nb_classifier_2 = NaiveBayesClassifier.train(train_set_2)
#print(nb_classifier_1.classify(gender_features_1('Slavko')))

print('NaiveBayesClassifier tocnost #1:', classify.accuracy(nb_classifier_1, test_set_1) * 100, '%')
nb_classifier_1.show_most_informative_features(10)
print('NaiveBayesClassifier tocnost #2:', classify.accuracy(nb_classifier_2, test_set_2) * 100, '%')
nb_classifier_2.show_most_informative_features(10)
print()

'''
Max Entropy
'''

me_classifier_1 = MaxentClassifier.train(train_set_1)
me_classifier_2 = MaxentClassifier.train(train_set_2)
#print(me_classifier_1.classify(gender_features_1('Željkica')))

print('MaxentClassifier tocnost #1:', classify.accuracy(me_classifier_1, test_set_1) * 100, '%')
me_classifier_1.show_most_informative_features(10)
print('MaxentClassifier tocnost #2:', classify.accuracy(me_classifier_2, test_set_2) * 100, '%')
me_classifier_2.show_most_informative_features(10)
print()

'''
Decision Tree
'''

tree_classifier_1 = DecisionTreeClassifier.train(train_set_1)
tree_classifier_2 = DecisionTreeClassifier.train(train_set_2)

print('DecisionTreeClassifier tocnost #1:', classify.accuracy(tree_classifier_1, test_set_1) * 100, '%')
print('DecisionTreeClassifier tocnost #2:', classify.accuracy(tree_classifier_2, test_set_2) * 100, '%')
#tree_classifier_1.show_most_informative_features(10) # nema tu funkciju
