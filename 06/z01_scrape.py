import requests
import bs4

'''
Scrape
'''

slova = 'A B C Č Ć D DŽ Đ E F G H I J K L LJ M N NJ O P R S Š T U V Z Ž'
slova = slova.split()
url_muska = 'http://www.trudnoca.net/p/muska_imena/'
url_zenska = 'http://www.trudnoca.net/p/zenska_imena/'
imena_m = []
imena_z = []

for slovo in slova:
    url_m = url_muska + slovo + '/'
    url_z = url_zenska + slovo + '/'

    for url in [url_m, url_z]:

        html_page = requests.get(url).content
        soup = bs4.BeautifulSoup(html_page)

        links = soup.find_all('a')
        
        for link in links:
            if link.has_attr('href') and link['href'][:13] == '/muska_imena/':
                ime = link.contents[0]
                if ',' in ime:
                    ime = ime.split(',')
                    ime = [i.strip() for i in ime]
                    imena_m += ime
                else:
                    imena_m.append(ime)
            elif link.has_attr('href') and link['href'][:14] == '/zenska_imena/':
                ime = link.contents[0]
                if ',' in ime:
                    ime = ime.split(',')
                    ime = [i.strip() for i in ime]
                    imena_z += ime
                else:
                    imena_z.append(ime)

    print('Obradjeno slovo', slovo)
                    

with open('muska_imena.txt', 'w', encoding='utf-8') as outfile:
    outfile.write('\n'.join(imena_m))

with open('zenska_imena.txt', 'w', encoding='utf-8') as outfile:
    outfile.write('\n'.join(imena_z))    
