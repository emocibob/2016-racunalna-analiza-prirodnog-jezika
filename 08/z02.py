"""
 2. Napišite gramatiku s uzorcima tagova i istrenirjte chunk
    parser za imeničke fraze koje sadrže gerund (npr. the/DT 
    receiving/VBG end/NN, assistant/NN managing/VBG editor/NN)
"""
import nltk

sentence = [("the", "DT"), ("receiving", "VBG"),  ("end", "NN"), ("assistant", "NN"), ("managing", "VBG"), ("editor", "NN")]
grammar = "NP: {<DT|NN><VBG><NN>}"

cp = nltk.RegexpParser(grammar)
result = cp.parse(sentence)
print(result)
