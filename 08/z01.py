"""
 1. Napišite gramatiku s uzorcima tagova i istrenirajte chunk parser
    za imeničke fraze koje sadrže imenice u množini (npr. many/JJ
    researchers/NNS, two/CD weeks/NNS, both/DT new/JJ positions/NNS)
"""
import nltk

sentence = [("many", "JJ"), ("researchers", "NNS"), ("two", "CD"), ("weeks", "NNS"), ("both", "DT"), ("new", "JJ"), ("positions", "NNS")]
grammar = "NP: {<DT|CD>?<JJ>*<NNS>}"

cp = nltk.RegexpParser(grammar)
result = cp.parse(sentence)
print(result)
