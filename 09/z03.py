"""
 3. naučite bigramski i trigramski chunker na train setu iz
    conll2000 korpusa s uključenim tipom 'VP' i evaluirajte
    na test setu iz istog  korpusa s uključenim tipom 'VP'
"""
import nltk


class BigramChunker(nltk.ChunkParserI):
    def __init__(self, train_sents):
        train_data = [[(t,c) for w,t,c in nltk.chunk.tree2conlltags(sent)]
                      for sent in train_sents]
        self.tagger = nltk.BigramTagger(train_data)

    def parse(self, sentence):
        pos_tags = [pos for (word,pos) in sentence]
        tagged_pos_tags = self.tagger.tag(pos_tags)
        chunktags = [chunktag for (pos, chunktag) in tagged_pos_tags]
        conlltags = [(word, pos, chunktag) for ((word,pos),chunktag)
                     in zip(sentence, chunktags)]
        return nltk.chunk.conlltags2tree(conlltags)

class TrigramChunker(nltk.ChunkParserI):
    def __init__(self, train_sents):
        train_data = [[(t,c) for w,t,c in nltk.chunk.tree2conlltags(sent)]
                      for sent in train_sents]
        self.tagger = nltk.TrigramTagger(train_data)

    def parse(self, sentence):
        pos_tags = [pos for (word,pos) in sentence]
        tagged_pos_tags = self.tagger.tag(pos_tags)
        chunktags = [chunktag for (pos, chunktag) in tagged_pos_tags]
        conlltags = [(word, pos, chunktag) for ((word,pos),chunktag)
                     in zip(sentence, chunktags)]
        return nltk.chunk.conlltags2tree(conlltags)

    
train_set = nltk.corpus.conll2000.chunked_sents('train.txt', chunk_types=['VP'])
test_set = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['VP'])

b_chunker = BigramChunker(train_set)
t_chunker = TrigramChunker(train_set)

print(" ** Bigram **")
print(b_chunker.evaluate(test_set))
print(" ** Trigram **")
print(t_chunker.evaluate(test_set))
