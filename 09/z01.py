"""
 1. ispiši 114. rečenicu iz conll2000 sa svim tipovima chunka
    i 120. tako da se u chunkove stavljaju samo tipovi vp
"""
from nltk.corpus import conll2000

print(conll2000.chunked_sents('train.txt')[113])
print(conll2000.chunked_sents('train.txt', chunk_types=['VP'])[119])
