"""
 2. chunkove s vježbi od prošli put, evaluirajte 
    u odnosu na testni skup u korpusu
"""
import nltk

grammars = ["NP: {<DT|CD>?<JJ>*<NNS>}",
           "NP: {<DT|NN><VBG><NN>}",
           """NP: {<NNP><CC><NNP>}
           {(<DT><PRP\$>)?<NN>?<NNS><CC><NNS>}"""]

for grammar in grammars:
    cp = nltk.RegexpParser(grammar)
    test_sents = nltk.corpus.conll2000.chunked_sents('test.txt')
    print("Gramatika:", grammar)
    print(cp.evaluate(test_sents))

