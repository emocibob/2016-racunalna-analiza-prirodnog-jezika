"""
 * “Istrenirajte” unigramski i bigramski tager na podacima za treniranje
   te ih primjenite na jednoj od rečenica iz podataka za treniranje;
   evaluirajte ih na podacima za testiranje
"""
import nltk
from nltk.corpus import brown

brown_tagged_sents = brown.tagged_sents(categories='fiction')

limit = int(len(brown_tagged_sents) * 0.9)
train = brown_tagged_sents[:limit]
test = brown_tagged_sents[limit:]    

unigram_tagger = nltk.UnigramTagger(train)
bigram_tagger = nltk.BigramTagger(train)

print(unigram_tagger.tag(train[10]))
print(bigram_tagger.tag(train[10]))

print(unigram_tagger.evaluate(test))
print(bigram_tagger.evaluate(test))
