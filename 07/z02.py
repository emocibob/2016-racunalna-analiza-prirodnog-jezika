"""
 * “Istrenirajte” DefaultTagger na rečenici: “Once upon a time in my younger year
   and in the dawn of this century I wrote ”, a kao parametar navedite najčešći
   tag kojeg ste pronašli u prethodnom koraku
"""
import nltk
from nltk.corpus import brown

rec = "Once upon a time in my younger years and in the dawn of this century I wrote"
tokens = nltk.word_tokenize(rec)

default_tagger = nltk.DefaultTagger('NN')
print(default_tagger.tag(tokens))
