import requests
import bs4
from datetime import datetime


def get_reviews(base_url, limit, prefix):

    print('Sakupljanje za:', prefix, '| Limit:', limit)
    
    reviews_counter = 0
    page_counter = 0
    
    while reviews_counter < limit: # stranica ima max 10 recenzija

        page_counter += 1
        url = base_url + str(page_counter)
        html_page = requests.get(url).content
        soup = bs4.BeautifulSoup(html_page, 'html5lib')

        reviews = soup.find_all('div', {'class' : 'review'})
        if len(reviews) > 0:
         
            for review in reviews:

                reviews_counter += 1

                date_posted = review.find('span', {'class' : 'review-date'}) # e.g. 'on May 25, 2016'
                date_posted = date_posted.get_text()[3:] # e.g. 'May 25, 2016'
                date_posted = datetime.strptime(date_posted, '%B %d, %Y') # u datetime objekt
                date_posted = date_posted.strftime('%d-%m-%Y') # e.g. 25-05-2016

                review_text = review.find('span', {'class' : 'review-text'}).get_text()
                file_name = prefix + '_amazon-kindle-reviews_' + date_posted + '_' + str(reviews_counter).rjust(4, '0')
                with open('korpus/' + file_name + '.txt', 'w', encoding='utf-8') as outfile:
                    outfile.write(review_text)

            print('  Sakupljeno', reviews_counter, 'recenzija')
                    
    return

url_5_star_reviews = 'http://www.amazon.com/Amazon-Kindle-Paperwhite-6-Inch-4GB-eReader/product-reviews/B00OQVZDJM/ref=cm_cr_arp_d_paging_btm_2?ie=UTF8&filterByStar=five_star&showViewpoints=0&pageNumber=' # na kraju dodje broj stranice
url_1_star_reviews = 'http://www.amazon.com/Amazon-Kindle-Paperwhite-6-Inch-4GB-eReader/product-reviews/B00OQVZDJM/ref=cm_cr_getr_d_paging_btm_2?ie=UTF8&filterByStar=one_star&showViewpoints=0&pageNumber=' # na kraju dodje broj stranice

get_reviews(url_5_star_reviews, 110, 'pos')
get_reviews(url_1_star_reviews, 110, 'neg')
