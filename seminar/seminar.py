from string import punctuation
from random import shuffle
import nltk
from os import listdir

'''
znacajke (trebalo bi ic u reader.py)
'''

with open('leksicki_resurs.txt') as infile:
    resurs = infile.read()

resurs = resurs.split('\n')

pos_dict = {}
neg_dict = {}

# setaj redak po redak
for redak in resurs:
    # dohvati rijec i polaritet
    rijec, pol = redak.split()[0], int(redak.split()[-1]) # pretpostavlja jednu rijec
    if pol > 0:
        pos_dict[rijec] = pol
    if pol < 0:
        neg_dict[rijec] = abs(pol)

svi_dict = {}

# key je rijec, value je pol
for key, value in pos_dict.items():
    svi_dict[key] = ('pos', value)
for key, value in neg_dict.items():
    svi_dict[key] = ('neg', value)

'''
funkcije za izvlacenje znacajki iz teksta (ovo treba ic u feature_extractors.py)
'''

def features_pos(tekst):

    tekst = tekst.split() # napravi listu
    result_pos = {}

    for rijec, pol in pos_dict.items():
        br_pojava = tekst.count(rijec)
        result_pos[rijec] = pol * br_pojava

    # provjera za svaki slucaj
    if len(result_pos) == len(pos_dict):
        return result_pos
    else:
        return None

def features_neg(tekst):

    tekst = tekst.split() # napravi listu
    result_neg = {}

    for rijec, pol in neg_dict.items():
        br_pojava = tekst.count(rijec)
        result_neg[rijec] = pol * br_pojava

    # provjera za svaki slucaj
    if len(result_neg) == len(neg_dict):
        return result_neg
    else:
        return None
    
def features_all(tekst):

    result_all = {}

    f_pos = features_pos(tekst)
    f_neg = features_neg(tekst)

    for k, v in f_pos.items():
        result_all[k + '_pos'] = v
    for k, v in f_neg.items():
        result_all[k + '_neg'] = v

    return result_all

'''
ucitavanje korpusa (trebao bi postojati i opis_korpusa.txt)
'''

files = listdir("korpus/")
txt_files = [f for f in files if f[-4:] == '.txt']

pos_reviews = []
neg_reviews = []

for review_file in txt_files:
    if review_file[:3] == 'pos':
    	with open('korpus/' + review_file, 'r') as infile:
    		pos_reviews.append(infile.read())
    elif review_file[:3] == 'neg':
    	with open('korpus/' + review_file, 'r') as infile:
    		neg_reviews.append(infile.read())
    
'''
ciscenje teksta
'''

def clean(tekst):
    # makni interpunkcijske znakove
    for p in punctuation:
        tekst = tekst.replace(p, '')

    return tekst

# sve u lowercase
pos_reviews = [clean(p.lower()) for p in pos_reviews]
neg_reviews = [clean(n.lower()) for n in neg_reviews]
all_reviews = pos_reviews + neg_reviews

print('POZ recenzija:', str(len(pos_reviews)))
print('NEG recenzija:', str(len(neg_reviews)))
print('UKUPNO recenzija:', str(len(all_reviews)))
all_reviews_join = ' '.join(all_reviews)
print('Ukupno pojavnica u korpusu:', len(set(all_reviews_join.split())))

'''
klasifikacija (i ovo bi islo u posebne py skripte koje bi generirale izlazne txt fileove)
'''

data_pos_f = [(features_pos(p), 'POS') for p in pos_reviews] + [(features_pos(n), 'NEG') for n in neg_reviews]
data_neg_f = [(features_neg(p), 'POS') for p in pos_reviews] + [(features_neg(n), 'NEG') for n in neg_reviews]
data_all_f = [(features_all(p), 'POS') for p in pos_reviews] + [(features_all(n), 'NEG') for n in neg_reviews]
shuffle(data_pos_f)
shuffle(data_neg_f)
shuffle(data_all_f)

korpus = [] # POS, NEG, ALL
for k in [data_pos_f, data_neg_f, data_all_f]:
    limit = int(len(k) * 0.9)
    train_set, test_set = k[:limit], k[limit:]
    korpus.append([train_set, test_set])

korpus[0].append("POS")
korpus[1].append("NEG")
korpus[2].append("ALL")

for k in korpus:

    print(' **', k[2], '**')

    classifier = nltk.NaiveBayesClassifier.train(k[0]) # train set
    print("Tocnost NaiveBayesClassifier:", nltk.classify.accuracy(classifier, k[1]) * 100, "%") # test set
    print(classifier.show_most_informative_features(15))

    classifier = nltk.DecisionTreeClassifier.train(k[0]) # train set
    print("Tocnost DecisionTreeClassifier:", nltk.classify.accuracy(classifier, k[1]) * 100, "%") # test set
    #print(classifier.show_most_informative_features(15)) # ovog nema?
    
    classifier = nltk.MaxentClassifier.train(k[0]) # train set
    print("Tocnost MaxentClassifier:", nltk.classify.accuracy(classifier, k[1]) * 100, "%") # test set
    print(classifier.show_most_informative_features(15))
