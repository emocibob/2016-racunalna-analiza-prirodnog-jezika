"""
3. Napiši skriptu koja pomoću regularnih izraza u datoteci traži i
   ispisuje brojeve telefona navedenih formata u izlaznudatoteku:
   
   TEL +385-(0)51-723-023
   Phone: (051) 723-023
   Tel (+385): 052-723-023
   <a href="contact.html">TEL</a> +385&thinsp;052&thinsp;723&thinsp;023
   
   (1 bod)
"""
import re


def nadji(regex, line):
    global text
    
    temp = re.search(regex, line)
    if temp:
        #print('regex', regex, 'daje:')
        #print(' ', temp.group())
        text += temp.group() + '\n'
        

data = '''TEL +385-(0)51-723-023
Phone: (051) 723-023
Tel (+385): 052-723-023
<a href="contact.html">TEL</a> +385&thinsp;052&thinsp;723&thinsp;023'''

data = data.split('\n')

text = ''

for line in data:

    #regex = '(TEL )?(Phone: )?(Tel )?\(?\+?[0-9]{3}\)?-?:? ?(\([0-9]\))?[0-9]{2,3}-[0-9]{3}(-[0-9]{3})?' # ovo je za prve tri linije
    regex = '(TEL )?(Phone: )?(Tel )?(<a href="contact\.html">TEL</a> )?\(?\+?[0-9]{3}\)?-?:? ?(&thinsp;)?(\([0-9]\))?[0-9]{2,3}-?(&thinsp;)?[0-9]{3}(-[0-9]{3})?(&thinsp;)?([0-9]{3})?'
    nadji(regex, line)


print(text)
outfile = open('z3.txt', 'w')
outfile.write(text)
outfile.close()
