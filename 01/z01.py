"""
 1. Napiši skriptu koja u datoteci mbox.txt traži linije formata:
 
    New Revision: 39772
    
    Izvuci broj od svake takve linije koristeći regularni izraz i  findall() metodu.
    Izračunaj prosjek brojeva i ispiši  prosjek. (0,5bod)
"""
import re

# ucitaj file
infile = open('mbox1.txt')
data = infile.read()
infile.close()

# nadji trazene uzorke
finds = re.findall('New Revision: [0-9]+', data)
print('Pronadjeno uzoraka:', len(finds))

# projsek brojeva
prosjek = 0
for f in finds:
    # nadji i dohvati broj
    broj = re.search('[0-9]+', f).group()
    prosjek += int(broj)

print('Prosjek brojeva:', prosjek/len(finds))
