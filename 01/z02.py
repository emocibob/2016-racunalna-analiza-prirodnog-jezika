"""
 2. Napiši skriptu koja pomoću regularnih izraza u  datoteci traži i
    ispisuje e-mail adrese  navedenih formata u izlaznudatoteku:
 
    lnacinovic@uniri.hr 
    lnacinovic (at) uniri.hr
    lnacinovic at uniri dot hr
    <script type="text/javascript">obfuscate(‘uniri.hr',‘lnacinovic’)</script>  

    (0,5 bod)
"""
import re

data = '''lnacinovic@uniri.hr
lnacinovic (at) uniri.hr
lnacinovic at uniri dot hr
<script type="text/javascript">obfuscate(‘uniri.hr',‘lnacinovic’)</script>'''

data = data.split('\n')

def nadji(regex, line):
    global text
    
    temp = re.search(regex, line)
    if temp:
        #print('regex', regex, 'daje:')
        #print(' ', temp.group())
        text += temp.group() + '\n'

outfile = open('z2.txt', 'w')
text = ''

for line in data:

    regex = '[a-z]+@[a-z\.]+'
    nadji(regex, line)

    regex = '[a-z]+ \(at\) [a-z\.]+'
    nadji(regex, line)
    
    regex = '[a-z]+ at [a-z]+ dot [a-z]+'
    nadji(regex, line)
    
    regex = "<script type=\"text/javascript\">obfuscate\(‘[a-z\.]+',‘[a-z]+’\)</script>"
    nadji(regex, line)

print(text)
outfile.write(text)
outfile.close()
