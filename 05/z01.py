"""
 * Odabrati jedan proizvod
 * Pronaći na Internetu pozitivne i negativne osvrte na taj proizvod
   (najmanje 20 iz svake kategorije)
 * Napraviti kategorizirani korpus (s pozitivnim i negativnim osvrtima)
 * Naučiti klasifikator, izračunati točnost i pronaći najinformativnije značajke
 * 3 boda
"""
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
import nltk
import random

def bag_of_words(words):
    return dict([(word, True) for word in words.split()])

"""
reader = CategorizedPlaintextCorpusReader('.', r'kindle_.*\.txt', cat_pattern=r'kindle_(\w+)\.txt')
print(reader.categories())
"""

with open("kindle_pos.txt", "r") as infile:
    pos = infile.read()
    pos = pos.split("\n\n")
    pos = [p.replace("\n", " ") for p in pos] # makni "\n"

with open("kindle_neg.txt", "r") as infile:
    neg = infile.read()
    neg = neg.split("\n\n")
    neg = [n.replace("\n", " ") for n in neg] # makni "\n"

data = [(bag_of_words(p), "POS") for p in pos] + [(bag_of_words(n), 'NEG') for n in neg]
random.shuffle(data)
limit = 36
train_set, test_set = data[:limit], data[limit:]

classifier = nltk.NaiveBayesClassifier.train(train_set)
print("Tocnost", nltk.classify.accuracy(classifier, test_set) * 100, "%")

print(classifier.show_most_informative_features(5))
