# 2016 Racunalna analiza prirodnog jezika

Repozitorij za vježbe iz kolegija *Računalna analiza prirodnog jezika ak. god. 2015/2016* (Odjel za informatiku, Sveučilište u Rijeci).
Kod u repozitoriju pisan je za Python 3.

Autor: Edvin Močibob ([edvin.mocibob@gmail.com](mailto:edvin.mocibob@gmail.com))