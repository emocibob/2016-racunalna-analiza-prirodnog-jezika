"""
* Iz datoteke sadrzaj.txt izvadite sve html tagove i interpunkcijske znakove
  i sačuvajte takav sadržaj u datoteku sadrzaj_procisceni.txt (0,5 bodova)
"""
import requests
import bs4
from string import punctuation

infile = open('sadrzaj.txt', 'r')
sadrzaj = infile.read()
infile.close()

soup = bs4.BeautifulSoup(sadrzaj)

# makni tagove
text = soup.get_text()

# makni interpunkcijske znakove
for p in punctuation:
    text = text.replace(p, '')

# makni prazne linije
data = ''
for line in text.split('\n'):
    if line.strip() != '':
        line = line.strip() # makni prazna mjesta na početku i kraju linije
        data += line + '\n'

outfile = open('sadrzaj_procisceni.txt', 'w')
outfile.write(data)
outfile.close()
