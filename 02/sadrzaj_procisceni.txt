Leonhard Euler  Wikipedija
documentdocumentElementclassName  documentdocumentElementclassNamereplace sclientnojss 1clientjs2
windowRLQwindowRLQpushfunctionmwconfigsetwgCanonicalNamespacewgCanonicalSpecialPageNamefalsewgNamespaceNumber0wgPageNameLeonhardEulerwgTitleLeonhard EulerwgCurRevisionId4554596wgRevisionId4554596wgArticleId24375wgIsArticletruewgIsRedirectfalsewgActionviewwgUserNamenullwgUserGroupswgCategoriesŠvicarski fizičariŠvicarski matematičariŠvicarski znanstveniciAstronomiwgBreakFramesfalsewgPageContentLanguagehrwgPageContentModelwikitextwgSeparatorTransformTablettwgDigitTransformTablewgDefaultDateFormatdmy hrwgMonthNamessiječnjaveljačeožujkatravnjasvibnjalipnjasrpnjakolovozarujnalistopadastudenogprosincawgMonthNamesShortsijveljožutrasvilipsrpkolrujlisstuprowgRelevantPageNameLeonhardEulerwgRelevantArticleId24375wgRequestId48148d5512efa1d27019599awgIsProbablyEditabletruewgRestrictionEditwgRestrictionMovewgWikiEditorEnabledModulestoolbartruedialogstruepreviewfalsepublishfalsewgBetaFeaturesFeatureswgMediaViewerOnClicktruewgMediaViewerEnabledByDefaulttruewgVisualEditorpageLanguageCodehrpageLanguageDirltrusePageImagestrueusePageDescriptionstruewgPreferredVarianthrwgRelatedArticlesnullwgRelatedArticlesUseCirrusSearchtruewgRelatedArticlesOnlyUseCirrusSearchfalsewgULSAcceptLanguageListhrhrhrenusenwgULSCurrentAutonymhrvatskiwgCategoryTreePageCategoryOptionsmode0hideprefix20showcounttruenamespacesfalsewgNoticeProjectwikipediawgCentralNoticeCategoriesUsingLegacyFundraisingfundraisingwgCentralAuthMobileDomainfalsewgWikibaseItemIdQ7604wgVisualEditorToolbarScrollOffset0mwloaderimplementuseroptionsfunctionjQuerymwuseroptionssetvarianthrmwloaderimplementusertokensfunction   jQuery
mwusertokensseteditTokenpatrolTokenwatchTokencsrfTokennomin
mwloaderloadmediawikipagestartupmediawikilegacywikibitsextcentralauthcentralautologinmmvheadextvisualEditordesktopArticleTargetinitextulsinitextulsinterfacemwMediaWikiPlayerloadermwPopUpMediaTransformextcentralNoticebannerControllerskinsvectorjs
Leonhard Euler
Izvor Wikipedija
Skoči na
orijentacija
traži
Leonhard Euler
Rođenje
15 travnja
1707
Basel
Švicarska
Smrt
18 rujna
1783
SanktPeterburg
Rusija
Državljanstvo
Švicarac
Polje
matematika
fizika
Institucija
Pruska akademija u Berlinu
Akademija znanosti u Sankt Peterburgu
Alma mater
Šveučilište u Baselu
Poznat po
Eulerov broj
Eulerova formula
Eulerov identitet
Leonhard Euler portret iz 1753 na kojem je vidljiv njegov problem s desnim okom
Sankt Peterburg 1826
Naslovnica Eulerovog djela
Methodus inveniendi lineas curvas
Pravilni dodekaedar
Leonhard Euler
Basel
15 travnja
1707
Petrograd
18 rujna
1783
švicarski
matematičar fizičar i astronom
Svoju znanstvenu djelatnost razvio je u
Berlinu
i
Petrogradu
gdje je držao katedru
fizike
i
matematike
Njegova aktivnost nije stala ni kada je oslijepio jer je tada diktirao svoje radove Napisao je oko 900 radova Razvio je
teoriju redova
uveo tzv
Eulerove integrale
riješio mnoge diferencijalne jednadžbe a u diferencijalnoj
geometriji
dao je prvu formulu zakrivljenosti ploha
Eulerov poučak
Posebno su važna dva njegova istraživanja u
hidrodinamici
gdje je razvio teoriju
turbina
Proučavao je širenje
zvuka
i
svjetlosti
1
Eulerov odbor
Švicarske
akademije
znanosti osnovan 1907 dobio je u zadatak objaviti cjelokupno Eulerovo djelo U 100 narednih godina objavljena su 84 toma enciklopedijskog formata Euler je najproduktivniji matematičar u povijesti Nakon njegove smrti
Sanktpetersburška
je akademija još punih 50 godina tiskala njegove neobjavljene radove
Sadržaj
1
Život
11
Sankt Peterburg
12
Berlin
13
Problem s vidom
14
Povratak u Rusiju
2
Doprinosi matematici i fizici
3
Izvori
Život
uredi VE
uredi
Euler se rodio u švicarskom gradu Baselu Otac Paul bio je
pastor
u
protestantskoj
crkvi a i majka Marguerite Brucker potjecala je iz svećeničke obitelji Euler je imao dvije mlađe sestre Anna Mariju i Mariju Magdalenu Nedugo nakon njegova rođenja obitelj je preselila u obližnji gradić Riehen gdje je Euler proveo djetinjstvo
Jedan od bliskih prijatelja obitelji bio je tada već u Europi priznati matematičar Johann Bernoulli a to je prijateljstvo zasigurno je bilo presudno za životni put Leonharda Eulera
Sa školskim obrazovanjem započeo je u Baselu gdje je živio s bakom po majci Već sa 14 godina upisao se na univerzitet Najprije prolazi temeljito opće obrazovanje a 1723 završava studij disertacijom u kojoj uspoređuje učenje
Descartesa
i
Newtona
Pokazuje sve veće zanimanje za matematiku i uočava rupe u svojem znanju pa moli Johanna Bernoullija za redovitu privatnu poduku No ovaj je pristao tek subotom ili nedjeljom poslijepodne odvojti nešto vremena kako bi mu odgovarao na pitanja i davao savjete što čitati i proučavati Iskusni Bernoulli je brzo uočio Eulerovu izuzetnu nadarenost te nazreo njegov silni
znanstveni
potencijal Kad je Euler počeo studirati
teologiju
učiti
grčki
latinski
i
hebrejski jezik
njegov se otac ponadao da će sin poći njegovim stopama Nije Bernoulliju stoga bilo nimalo lako uvjeriti oca kako mu je sin sudbinski predodređen postati velikim matematičarom
2
Godine 1727 Euler završava svoju
doktorsku
disertaciju o širenju
zvuka
De Sono
Iz iste godine potječe njegovo prvo značajno priznanje
Pariška
akademija
dodijelila mu je drugu nagradu za rješenje problema o optimalnom smještavanju
jarbola
na
jedrenjak
3
Sankt Peterburg
uredi VE
uredi
Dva sina Johanna
Bernoullija Daniel
i Nicolas radili su na
Ruskoj
carskoj akademiji znanosti u
Sankt Petersburgu
ustanovi čiji je utemeljitelj
Petar Veliki
imao nakanu unaprijediti
obrazovanje
i znanost u zemlji te je povezati sa
Zapadnom Europom
Ruska je akademija zbog obilate novčane carske potpore i uvjeta rada bila privlačna za mlade i ambiciozne europske znanstvenike I kada je u srpnju 1726 Nikolas Bernoulli umro od upale slijepog crijeva i Daniel pozvao na upražnjeno mjesto obiteljskog prijatelja Leonharda Eulera ovaj se nije mnogo dvoumio U Sankt Petersburgu je stigao 17 svibnja 1727 te se zaposlio na medicinskom odjelu Akademije
Nedugo nakon njegova dolaska umire carica
Katarina I
i prijestolje preuzima dvadesetjednogodišnji
Petar II
Rusko je
plemstvo
sumnjičavo prema stranim znanstvenicima što stvara probleme i umanjuje im carsku podršku No Petar II već 1730 umire i uvjeti se opet okrenu nabolje Godine 1731 Euler postaje
profesorom
fizike
Zanimljivo je da je u tom vremenu napisao dvotomnu
Mehaniku
knjigu o teoriji glazbe te djelo
Scientia navalis
u kojoj izlaže znanja iz
hidrodinamike
gradnje brodova
i
navigacije
Dvije godine kasnije nakon što je Daniel Bernoulli napustio Rusiju i vratio se u Basel Euler ga nasljeđuje na mjestu voditelja matematičkog odjela
Euler se ženi 1734 Njegova je odabranica Katharina Gsell kći švicarskog slikara Georgea Gsella koji je živio u Sankt Peterburgu Uskoro su kupili kuću na obali rijeke
Neve
Imali su trinaestoro djece od kojih je samo petoro preživjelo djetinjstvo a samo je troje nadživjelo oca Sin Johann Albrecht je Eulerov jedini potomak koji je slijedio očeve stope bavio se matematikom i bio član Akademije Euler je imao 21 unuče Volio je djecu i pričao je kako je do svojih najvećih otkrića došao dok je držao bebu na rukama i dok su se oko njega motali drugi mališani
4
Berlin
uredi VE
uredi
Zabrinuti zbog učestalih nemira u Rusiji Eulerovi razmišljaju o napuštanju Sankt Petersburga Objeručke prihvaćaju ponudu
pruskog
kralja
Frederika II
da prijeđe na Univerzitet u
Berlinu
i 19 lipnja 1741 obitelj seli u Berlin Euler je na Univerzitetu bio predvodnikom matematičkog odjela
U Berlinu Euler provodi sljedećih 25 godina U tom je periodu napisao preko 380 znanstvenih članaka i objavio svoja dva velika djela
Introductio in analysis infinitorum
i
Institutiones calculi differentialis
Tu je nastala i većina njegovih radova iz računa varijacija teorije specijalnih funkcija
diferencijalnih jednadžbi
astronomije
mehanike
Bio je član gotovo svih značajnijih akademija u Europi i dobitnik brojnih priznanja i nagrada
Iz toga je razdoblja osobito zanimljivo 200 pisama što ih je Euler pisao Frederikovoj nećakinji princezi od Anhalt–Dessaua kojoj je davao poduke Ta su pisma objavljena u knjizi s naslovom
Pisma njemačkoj princezi
o raznim područjima fizike i filozofije Knjiga je bila pravi matematički bestseler U njoj Euler izlaže i analizira niz problema iz matematike i fizike To čini na vrlo osobit i osoban način pa ga čitatelj kroz pisma može dobro upoznati razumjeti njegova religiozna i druga uvjerenja Knjiga ukazuje kako je Euler zbog dubokog razumijevanja cjeline prirodnih znanosti imao sposobnost jednostavnog tumačenja i složenih činjenica
5
Problem s vidom
uredi VE
uredi
Usprkos iznimnom utjecaju što ga je imao u Berlinskoj akademiji Euler se sukobljavao s Frederikom II čiji je ljubimac
Voltaire
zauzeo središnje mjesto u njegovu društvu Počeli su i problemi s
vidom
1738 oslijepio je na desno
oko
Sljepoća je bila posljedica trovanja zbog gnojnog čira Usprkos svemu nastavio je raditi s jednakim žarom posvetio se čak izradi
atlasa
izradio je prvu pomorsku kartu Rusije zbirci karata za Sanktpetersburšku akademiju
Nije prošlo dugo vremena a
siva mrena
je prekrila i njegovo lijevo oko te je bio gotovo potpuno slijep Koliko ga god vrlo slab vid ometao u radu zbog svoje fantastičnog
pamćenja
nastavio se punim žarom baviti znanošću Biografi u želji da prikažu Eulerovu memoriju često navode kako je bio u stanju napamet bez zamuckivanja izrecitirati cijelu
Virgilijevu
Eneidu
i kako je za svaku stranicu mogao reći koji je redak na njoj prvi a koji posljednji Nakon gubitka vida Euler je stvorio gotovo pola svojeg znanstvenog opusa između ostalog trotomno djelo o
integralnom računu
Povratak u Rusiju
uredi VE
uredi
1766 Euler prihvaća poziv ruske carice
Katarine II
i vraća se u Sankt Petersburg gdje provodi ostatak života Nakon neuspjele operacije oka 1771 Euler je potpuno oslijepio Iste godine zatiče ga još jedna nesreća U velikom
požaru
što je zahvatio Sankt Petersburg nestao je i njegov dom a iz kućice u plamenu jedva ga je uspio izvući neki znanac Bio je to početak Eulerova tužnog kraja Nakon 40 godina zajedničkog života 1773 umire mu žena Nesposoban brinuti se o sebi on nakon tri godine oženi pokojničinu sestričnu Abigail Gsell 18 rujna 1783 Eulera zadesi izljev krvi u mozak i on umire Brzo i bezbolno Sahranjen je u Pskovu na Lazarevskom groblju
Doprinosi matematici i fizici
uredi VE
uredi
Matematička analiza
dugo je bila središnja točka njegova rada i interesa a svoje najznačajnije djelo
Uvod u analizu beskonačnosti
objavljuje 1748 U tom djelu Euler definira
funkciju
kao analitički izraz sastavljen nekom metodom od promjenjive vrijednosti i brojeva ili od konstantnih vrijednosti definira polinome
trigonometrijske funkcije
eksponencijalne funkcije
te njegovu suprotnu funkciju –
logaritamsku funkciju
6
Euler definira eksponencijalnu funkciju i na skupu
kompleksnih brojeva
te je povezuje sa trigonometrijskim funkcijama Za bilo koji realni broj vrijedi
Iz te formule proističe čuveni
Eulerov identitet
za koji mnogi smatraju budući da povezuje pet važnih matematičkih konstanti
e
1
i
π
i 0 najljepšom jednakošću cijele matematičke znanosti
Uz Eulerovo ime veže se čitav niz pojmova Osim oznake fx za standardni zapis realne
funkcije
1734 uveo je još oznaku i za
drugi korjen iz 1
1777 slovo e za zapis poznatog
Eulerovog broja
1727 oznaku Σ za
zbrajanje
1755 oznake Δ
sin
cos i mnoge druge Iako mu se to pripisuje on nije uveo oznaku π za omjer opsega i promjera kružnice ali je dosljednom upotrebom pridonio da bude prihvaćena
Evo nekih od formula i teorema koji dugujemo Euleru
Eulerova funkcija
Eulerovi brojevi
Eulerov pravac ortocentar središte opisane kružnice i težište nekog trokuta nalaze se na istom pravcu
Eulerova formula
za homogene funkcije
Eulerova formula zakrivljenosti plohe kristalografija
Eulerov integral druge vrste ili gamafunkcija
Eulerov integral prve vrste ili betafunkcija
Eulerovi kutovi
Još jedan Eulerov jednostavan ali značajan doprinos matematici koji se smatra temelj
topologije
a to je Eulerova poliedarska formula – jednakost koja povezuje broj vrhova V broj bridova B i broj strana svakog konveknog poliedra
V  B  S  2
Posljedica Eulerove poliedarske formule je postojanje točno pet pravilnih
poliedara
To su pravilni
tetraedar
heksaedar
kocka
oktaedar
dodekaedar
i
ikosaedar
Izvori
uredi VE
uredi
↑
Dunham William Euler The Master of Us All 1999 publisher The Mathematical Association of America
↑
James Ioan Remarkable Mathematicians From Euler to von Neumann publisher Cambridge 2002
↑
Branimir Dakić Leonhard Euler Matematika i škola
1
2007
↑
IR Gekker Leonhard Eulers family and descendants
↑
Letters of Voltaire and Frederick the Great Letter H 7434 1778 authorFrederick II of Prussia publisherBrentanos 1927
↑
Leonhard Euler wwwphyunirihr
2
2007
Dobavljeno iz
httpshrwikipediaorgwindexphptitleLeonhardEuleroldid4554596
Kategorije
Švicarski fizičari
Švicarski matematičari
Švicarski znanstvenici
Astronomi
Navigacijski izbornik
Osobni alati
Niste prijavljeni
Razgovor
Doprinosi
Otvori novi suradnički račun
Prijavi se
Imenski prostori
Članak
Razgovor
Inačice
Pogledi
Čitaj
uredi VE
uredi
Vidi stare izmjene
Više
Traži
Orijentacija
Glavna stranica
Kafić
Aktualno
Nedavne promjene
Slučajna stranica
Pomoć
Donacije
Ispisizvoz
Napravi zbirku
Preuzmi kao PDF
Inačica za ispis
Wikimedijini projekti
Zajednički poslužitelj
Pomagala
Što vodi ovamo
Povezane stranice
Postavi datoteku
Posebne stranice
Trajna poveznica
Podatci o stranici
Wikidata stavka
Citiraj ovaj članak
Drugi jezici
Адыгабзэ
Afrikaans
Alemannisch
አማርኛ
Aragonés
العربية
مصرى
অসমীয়া
Asturianu
Azərbaycanca
Башҡортса
Žemaitėška
Беларуская
Беларуская тарашкевіца‎
Български
বাংলা
Brezhoneg
Bosanski
Буряад
Català
Нохчийн
کوردیی ناوەندی
Čeština
Cymraeg
Dansk
Deutsch
Ελληνικά
English
Esperanto
Español
Eesti
Euskara
Estremeñu
فارسی
Suomi
Français
Frysk
Gaeilge
贛語
Galego
𐌲𐌿𐍄𐌹𐍃𐌺
עברית
हिन्दी
Fiji Hindi
Kreyòl ayisyen
Magyar
Հայերեն
Interlingua
Bahasa Indonesia
Ilokano
Ido
Íslenska
Italiano
日本語
La lojban
Basa Jawa
ქართული
Qaraqalpaqsha
Қазақша
ភាសាខ្មែរ
ಕನ್ನಡ
한국어
Latina
Lëtzebuergesch
Лезги
Limburgs
Lumbaart
Lietuvių
Latviešu
Malagasy
Македонски
മലയാളം
Монгол
मराठी
Bahasa Melayu
Mirandés
မြန်မာဘာသာ
Nederlands
Norsk nynorsk
Norsk bokmål
Occitan
Oromoo
ਪੰਜਾਬੀ
Polski
Piemontèis
پنجابی
Português
Română
Русский
Русиньскый
संस्कृतम्
Саха тыла
Sicilianu
Scots
Srpskohrvatski  српскохрватски
සිංහල
Simple English
Slovenčina
Slovenščina
Shqip
Српски  srpski
Basa Sunda
Svenska
Kiswahili
தமிழ்
తెలుగు
Тоҷикӣ
ไทย
Türkmençe
Tagalog
Türkçe
Татарчаtatarça
Українська
اردو
Oʻzbekchaўзбекча
Vepsän kel’
Tiếng Việt
Volapük
Winaray
Хальмг
მარგალური
ייִדיש
Yorùbá
中文
文言
Bânlâmgú
粵語
Uredi međuwikije
Datum i vrijeme posljednje promjene na ovoj stranici 27 lipnja 2015 u 2209
Tekst je dostupan pod licencijom
Creative Commons ImenovanjeDijeli pod istim uvjetima
dodatni uvjeti se mogu primjenjivati Pogledajte
Uvjete korištenja
za detalje
Zaštita privatnosti
Impresum
Uvjeti korištenja  Pravne napomene  Odricanje od odgovornosti
Razvojni programeri
Izjava o kolačićima
Prikaz za mobilne uređaje
windowRLQwindowRLQpushfunctionmwloaderstateextglobalCssJssitereadyextglobalCssJsuserreadyuserreadyusergroupsreadymwloaderloadextcitea11ymediawikitocmediawikiactionviewpostEditsitemediawikiusermediawikihidpimediawikipagereadymediawikisearchSuggestexteventLoggingsubscriberextgadgetnormalnebojerazlikammvbootstrapautostartextvisualEditortargetLoaderextwikimediaEventsextnavigationTimingschemaUniversalLanguageSelectorextulseventloggerextulsinterlanguage
windowRLQwindowRLQpushfunctionmwconfigsetwgBackendResponseTime74wgHostnamemw1249
