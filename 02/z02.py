"""
* Napravite skriptu koja će skinuti sve linkove s neke
  stranice i spremiti ih u datoteku. (0,25 bodova)
"""
import requests
import bs4

url = 'https://hr.wikipedia.org/wiki/Leonhard_Euler'
html_page = requests.get(url).content
soup = bs4.BeautifulSoup(html_page)

links = soup.find_all('a')
links_list = []
for link in links:
    if link.has_attr('href'):
        # dohvati samo linkove (href atribut)
        links_list.append(link['href'])
links_string = '\n'.join(links_list)

outfile = open('linkovi.txt', 'w')
outfile.write(links_string)
outfile.close()
