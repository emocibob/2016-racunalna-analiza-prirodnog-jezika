"""
* Napravite skriptu koja će skinuti sadržaj s neke stranice
  te ga spremiti u datoteku sadrzaj.txt. (0,25 boda)
"""
import requests
import bs4

url = 'https://hr.wikipedia.org/wiki/Leonhard_Euler'
html_page = requests.get(url).content
soup = bs4.BeautifulSoup(html_page)

outfile = open('sadrzaj.txt', 'w')
outfile.write(soup.prettify())
outfile.close()
